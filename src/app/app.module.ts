import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { AlertModule } from 'ngx-bootstrap/alert';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { ButtonsModule } from 'ngx-bootstrap/buttons';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { CarouselModule } from 'ngx-bootstrap/carousel';

import { AppComponent } from './app.component';
import { TopMenuBarComponent } from './components/top-menu-bar/top-menu-bar.component';
import { FooterBarComponent } from './components/footer-bar/footer-bar.component';

import { AuthComponent } from './components/auth/auth.component';
import { LoadingSpinnerComponent } from './shared/loading-spinner/loading-spinner.component';
import { UserComponent } from './components/user/user.component';
import { UserPageComponent } from './user-page/user-page.component';
import { HomePage } from './components/pages/home/home.page';
import { AboutPage } from './components/pages/about/about.page';
import { ContactPage } from './components/pages/contact/contact.page';
import { DictionaryPageComponent } from './components/pages/dictionary/dictionary.page';
import { ProductComponent } from './components/product/product.component';

@NgModule({
  declarations: [
    AppComponent,
    TopMenuBarComponent,
    FooterBarComponent,
    AuthComponent,
    LoadingSpinnerComponent,
    UserComponent,
    UserPageComponent,
    AboutPage,
    ContactPage,
    HomePage,
    DictionaryPageComponent,
    ProductComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    FontAwesomeModule,
    BsDropdownModule.forRoot(),
    AlertModule.forRoot(),
    ButtonsModule.forRoot(),
    TabsModule.forRoot(),
    CarouselModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
