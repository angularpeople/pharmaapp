import { Component, OnInit, OnDestroy } from '@angular/core';
import { faUserCircle } from '@fortawesome/free-solid-svg-icons';
import { AuthService } from '../auth/auth.service';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit, OnDestroy {
  private userSub: Subscription;
  isAuthentificated = false;
  userFullName = '';
  faUserCircle = faUserCircle;

  constructor(private authService: AuthService, private router: Router) { }

  ngOnInit() {
    this.userSub = this.authService.user.subscribe(user => {
      this.isAuthentificated = !!user;
      if(this.isAuthentificated){
          
      }
    });
  }

  onUserAccount(){
    if(this.isAuthentificated){
      this.router.navigate(['/user-page']);
    }
    else{
      this.router.navigate(['/auth']);
    }
  }

  ngOnDestroy() {
    this.userSub.unsubscribe();
  }
}
