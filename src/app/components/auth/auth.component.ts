import { Component, OnInit } from '@angular/core';
import { NgForm, EmailValidator } from '@angular/forms';
import { AuthService, AuthResponseData } from './auth.service';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { stringify } from 'querystring';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls : ['./auth.component.scss']
})
export class AuthComponent {
  isLoginMode = true;
  isLoading = false;
  errorMessage: string = null;
  error:string = null;

  constructor(private authService: AuthService, private router: Router) {}

  onSwitchMode(form: NgForm) {
    this.isLoginMode = !this.isLoginMode;
    this.errorMessage = null;
    form.reset();
  }

  onSubmit(form: NgForm){
    var isFormValid = this.formValidation(form);
    if(!isFormValid){
     return;
    }
    const email = form.value.email;
    const password = form.value.password;

    let authObs: Observable<AuthResponseData>;

    this.isLoading = true;

    if (this.isLoginMode) {
      authObs = this.authService.login(email, password);
    } else {
      authObs = this.authService.signup(email, password);
    }

    authObs.subscribe(
      resData => {
        console.log(resData);
        this.isLoading = false;
        this.router.navigate(['/user-page']);
      },
      errorMessage => {
        console.log(errorMessage);
        this.error = errorMessage;
        this.isLoading = false;
      }
    );

    form.reset();
  }

  formValidation(form: NgForm){

    var countErrors = 0;
      if(!this.isEmailValid(form.value.email)){
        this.errorMessage = "Email is inccorect!";
        countErrors++;
      }
      if(!this.isPasswordValid(form.value.password)){
        this.errorMessage = "Password is incorrect!";
        countErrors++;
      }
      if(!this.isLoginMode && !this.isFullNameValid(form.value.fullName))
      {
        this.errorMessage = "Full name is inccorect!";
        countErrors++;
      }
      if(countErrors>1){
        this.errorMessage = "Multiple fields are incorrect!";
        return false;
      }
      else if(countErrors == 1){
        return false;
      }
      return true;
  }

  isEmailValid(email:string){
    return /^[a-zA-Z0-9]+[._-]{0,1}[a-zA-Z0-9]+@[a-zA-Z0-9]+\.[a-zA-Z]{2,}$/.test(email);
  }
  isPasswordValid(password:string){
    if(password.indexOf(" ") !== -1 || password.length < 6){
      return false;
    }
    return true;
  }
  isFullNameValid(fullName:string){
    return /^[A-Za-z]+[,.' -]*[A-Za-z]+$/.test(fullName);
  }
}
