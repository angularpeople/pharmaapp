import { Component, OnInit } from '@angular/core';
import { faFacebookSquare } from '@fortawesome/free-brands-svg-icons';
import { faInstagram } from '@fortawesome/free-brands-svg-icons';
import { faTwitterSquare } from '@fortawesome/free-brands-svg-icons';


@Component({
    selector: 'app-footer-bar',
    templateUrl: './footer-bar.component.html',
    styleUrls: ['./footer-bar.component.scss']
  })
  export class FooterBarComponent implements OnInit {

    constructor() { }

    faFacebookSquare = faFacebookSquare;
    faInstagram = faInstagram;
    faTwitterSquare = faTwitterSquare;

    ngOnInit(): void {
    }

}
