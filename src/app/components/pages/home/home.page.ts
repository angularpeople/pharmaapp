import { Component, OnInit, OnDestroy } from '@angular/core';
import { EndpointService } from 'src/app/shared/loading-spinner/services/endpoint.service';
import { Observable, Subscription } from 'rxjs';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss']
})
export class HomePage implements OnInit, OnDestroy {

  private products$: Subscription;
  public products;

  constructor(private endpointService: EndpointService) { }

  ngOnInit(): void {
    this.products$ = this.endpointService.getAllProducts().subscribe(products => {
      this.products = products;
    });
  }

  ngOnDestroy(): void {
    this.products$.unsubscribe();
  }

  itemsPerSlide = 3;
  singleSlideOffset = false;
  noWrap = false;
 
  slides = [
    {image: 'https://comenzi.farmaciatei.ro/images/campaigns/alevia-20-mai-iunie-chb-7025.jpg'},
    {image: 'https://comenzi.farmaciatei.ro/images/campaigns/centrum-pachetomat-chb-333.png'},
    {image: 'https://comenzi.farmaciatei.ro/images/campaigns/la-roche-posay-anthelios-20-iunie-chb-9551.jpg'},
    {image: 'https://comenzi.farmaciatei.ro/images/campaigns/sanosan-20-mai-iunie-chb-1943.jpg'},
    {image: 'https://comenzi.farmaciatei.ro/images/campaigns/fixoderm-lo-iun-chb-1924.jpg'},
    {image: 'https://comenzi.farmaciatei.ro/images/campaigns/la-roche-posay-20-iunie-chb-8376.jpg'}
  ];

}
