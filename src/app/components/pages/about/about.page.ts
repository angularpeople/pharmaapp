import { Component, OnInit } from '@angular/core';
import { faFacebookSquare } from '@fortawesome/free-brands-svg-icons';
import { faInstagram } from '@fortawesome/free-brands-svg-icons';
import { faTwitterSquare } from '@fortawesome/free-brands-svg-icons';

@Component({
  selector: 'app-about',
  templateUrl: './about.page.html',
  styleUrls: ['./about.page.scss']
})
// tslint:disable-next-line:component-class-suffix
export class AboutPage implements OnInit {

  constructor() { }

  faFacebookSquare = faFacebookSquare;
  faInstagram = faInstagram;
  faTwitterSquare = faTwitterSquare;

  ngOnInit(): void {
  }

}
