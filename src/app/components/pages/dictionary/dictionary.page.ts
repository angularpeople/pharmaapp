import { Component, OnInit, OnDestroy } from "@angular/core";
import { Subscription } from "rxjs";
import { EndpointService } from "src/app/shared/loading-spinner/services/endpoint.service";
import { faAngleDoubleRight } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: "app-dictionary",
  templateUrl: "./dictionary.page.html",
  styleUrls: ["./dictionary.page.scss"],
})
export class DictionaryPageComponent implements OnInit, OnDestroy {
  private words$: Subscription;
  public words;

  constructor(private endpointService: EndpointService) {}

  faAngleDoubleRight = faAngleDoubleRight;

  ngOnInit(): void {
    this.words$ = this.endpointService.getAllWords().subscribe((words) => {
      this.words = words;
    });
  }

  ngOnDestroy(): void {
    this.words$.unsubscribe();
  }
}
