export interface Product {
    id: number;
    title: string;
    price: number;
    imageSrc: string;
    description: string;
    category: string;
  }
  