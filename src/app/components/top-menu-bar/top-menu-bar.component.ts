import { Component, OnInit } from '@angular/core';
import { faBars } from '@fortawesome/free-solid-svg-icons';
import { faSearch } from '@fortawesome/free-solid-svg-icons';
import { faUser } from '@fortawesome/free-regular-svg-icons';
import { faShoppingCart } from '@fortawesome/free-solid-svg-icons';
import { RouterModule, Router } from '@angular/router';

@Component({
  selector: 'app-top-menu-bar',
  templateUrl: './top-menu-bar.component.html',
  styleUrls: ['./top-menu-bar.component.scss']
})
export class TopMenuBarComponent implements OnInit {

  constructor(private routes: Router) { }

  faBars = faBars;
  faSearch = faSearch;
  faUser = faUser;
  faShoppingCart = faShoppingCart;
  //1 added variables for user button
  isAuthentificated = false;
  userFullName = '';


  ngOnInit(): void {
  }

  onUserAccount(){
    if(this.isAuthentificated){
      this.routes.navigate(['user-page']);
    }
    else{
      this.routes.navigate(['auth']);
    }
  }

}
