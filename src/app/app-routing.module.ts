import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { AuthComponent } from './components/auth/auth.component';
import { UserPageComponent } from './user-page/user-page.component';
import { AboutPage } from './components/pages/about/about.page';
import { ContactPage } from './components/pages/contact/contact.page';
import { HomePage } from './components/pages/home/home.page';
import { DictionaryPageComponent } from './components/pages/dictionary/dictionary.page';


const routes: Routes = [
{ path: 'auth', component: AuthComponent },
{ path: 'home', component: HomePage },
{ path: 'user', component: UserPageComponent},
{ path: 'about', component: AboutPage},
{ path: 'contact', component: ContactPage},
{ path: 'dictionary', component: DictionaryPageComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
