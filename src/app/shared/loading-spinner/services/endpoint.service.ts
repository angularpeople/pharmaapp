import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: "root",
})
export class EndpointService {
  constructor(private http: HttpClient) {}

  getAllProducts() {
    return this.http.get("/products-endpoint");
  }

  getAllWords() {
    return this.http.get("/dictionary-endpoint");
  }
}
